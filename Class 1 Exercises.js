/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

let small = 13;
let smallPrice = 16.99;
let big = 17;
let bigPrice = 19.99

let areaSmall = Math.PI * (small/2) ** 2;
let areaBig = Math.PI * (big/2) ** 2;

console.log("[MATH] 1.");
console.log(`area of ${small} inch pizza ${areaSmall}`);
console.log(`area of ${big} inch pizza ${areaBig}`);
console.log("\n");

// 2. What is the cost per square inch of each pizza?

console.log("[MATH] 2.");
console.log(`cost per inch of ${small} inch pizza ${smallPrice/areaSmall}`);
console.log(`cost per inch of ${big} inch pizza ${bigPrice/areaBig}`);
console.log("\n");

// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

let min = 1;
let max = 13;

// get random number from min - max and round to whole number
let card = Math.floor((Math.random() * max) + min);

console.log("[MATH] 3.");
console.log(`random card value is ${card}`);
console.log("\n");

// 4. Draw 3 cards and use Math to determine the highest
// card

let maxValue = 0;
let card1 = Math.floor((Math.random() * max) + min);
let card2 = Math.floor((Math.random() * max) + min);
let card3 = Math.floor((Math.random() * max) + min);

// find maximum
console.log("[MATH] 4.");
console.log(`highest card value is ${Math.max(card1, card2, card3)}`);
console.log("\n");

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

let firstName = "John";
let lastName = "Doe";
let streetAddress = "P.O. Box 123";
let city = "Seattle";
let state = "WA";
let zipCode = 12345;

let label = `${firstName} ${lastName}\n${streetAddress}\n${city}, ${state} ${zipCode}`;

console.log("[ADDRESS] 1.");
console.log(`${label}`);
console.log("\n");

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring

// split by newline char
let splitString = label.split("\n", 1);

console.log("[ADDRESS] 2.");
console.log(`Found name: ${splitString}`);
console.log("\n");

/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:
const startDate = new Date(2020, 1, 1);
const endDate = new Date(2020, 4, 1);

// get averages of time elapsed between time in seconds, use that to init Date object
let middleDiffMSeconds = (startDate.getTime() + endDate.getTime())/2;
let middleDate = new Date(middleDiffMSeconds);

console.log("[Date] 1.");
console.log(`Middle date: ${middleDate}`);
console.log("\n");